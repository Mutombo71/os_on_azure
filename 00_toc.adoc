= install openshift in azure
Dorian A. Redak <dorian.redak@gmail.com>
v0.1 2021-09-14
:description: a guide for installing red hat openshift cluster in microsoft azure
:toc:

:conf: conf
:pics: pics

IMPORTANT: record the id and tenantID field.

include::01_az_in_container.adoc[]

include::02_openshift_installer_vm_preparation.adoc[]

include::03_os_installation.adoc[]

include::80_post_installation_tasks.adoc[]

include::90_troubleshooting.adoc[]

== documentation
- https://docs.openshift.com/container-platform/4.8/installing/installing_azure/installing-azure-account.html#installing-azure-account[RH docs - os on azure]
- https://docs.microsoft.com/en-us/cli/azure/network/dns/zone?view=azure-cli-latest[create a dns zone in azure]
